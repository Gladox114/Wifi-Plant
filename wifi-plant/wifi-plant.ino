// Includes
// --------
// DS1307 RTC library using I2C
#include "RTClib.h"
// OLED Display library using I2C
#include <Arduino.h>
#include <U8g2lib.h>
// EEPROM to write/read offline data
#include <EEPROM.h>

// #define U8G2_WITH_FONT_ROTATION
// #define U8G2_WITH_INTERSECTION
// #define U8G2_WITH_CLIP_WINDOW_SUPPORT

// Definitions
// -----------
// -RTC-
// DS1307 RTC Library class
RTC_DS1307 rtc;
DateTime currentTime;
DateTime lastTime;
// Day definitions
char daysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
// -Oled-
// OLED Display library class
U8G2_SH1106_128X64_NONAME_1_HW_I2C u8g2(U8G2_R0, /* reset=*/ U8X8_PIN_NONE);
// Heart modes
enum HEARTMODE {
  EMPTY,
  HALF,
  FULL
};
// Heart Animations
enum HEARTSANIMATIONMODE {
  STATIC,
  CRITICAL,
  SPIN // for fun, make them
};
// XBM pictures
#define heartShell_bits_width 34
#define heartShell_bits_height 34
unsigned char heartShell_bits[] U8X8_PROGMEM = {
   0xc0, 0x0f, 0xc0, 0x0f, 0x00, 0xc0, 0x0f, 0xc0, 0x0f, 0x00, 0x20, 0x10,
   0x20, 0x10, 0x00, 0x18, 0x60, 0x18, 0x60, 0x00, 0x18, 0x60, 0x18, 0x60,
   0x00, 0x04, 0x80, 0x04, 0x80, 0x00, 0x03, 0x00, 0x03, 0x00, 0x03, 0x03,
   0x00, 0x03, 0x00, 0x03, 0x03, 0x00, 0x00, 0x00, 0x03, 0x03, 0x00, 0x00,
   0x00, 0x03, 0x03, 0x00, 0x00, 0x00, 0x03, 0x03, 0x00, 0x00, 0x00, 0x03,
   0x03, 0x00, 0x00, 0x00, 0x03, 0x03, 0x00, 0x00, 0x00, 0x03, 0x03, 0x00,
   0x00, 0x00, 0x03, 0x03, 0x00, 0x00, 0x00, 0x03, 0x03, 0x00, 0x00, 0x00,
   0x03, 0x03, 0x00, 0x00, 0x00, 0x03, 0x04, 0x00, 0x00, 0x80, 0x00, 0x18,
   0x00, 0x00, 0x60, 0x00, 0x18, 0x00, 0x00, 0x60, 0x00, 0x20, 0x00, 0x00,
   0x10, 0x00, 0xc0, 0x00, 0x00, 0x0c, 0x00, 0xc0, 0x00, 0x00, 0x0c, 0x00,
   0x00, 0x01, 0x00, 0x02, 0x00, 0x00, 0x06, 0x80, 0x01, 0x00, 0x00, 0x0e,
   0xc0, 0x01, 0x00, 0x00, 0x0c, 0xc0, 0x00, 0x00, 0x00, 0x10, 0x20, 0x00,
   0x00, 0x00, 0x60, 0x18, 0x00, 0x00, 0x00, 0x60, 0x18, 0x00, 0x00, 0x00,
   0x80, 0x04, 0x00, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x03,
   0x00, 0x00 };
#define heartLHalf_width 15
#define heartLHalf_height 27
static unsigned char heartLHalf_bits[] = {
   0xf0, 0x00, 0xf8, 0x01, 0xf8, 0x01, 0xfe, 0x07, 0xff, 0x0f, 0xff, 0x0f,
   0xff, 0x7f, 0xff, 0x7f, 0xff, 0x7f, 0xff, 0x7f, 0xff, 0x7f, 0xff, 0x7f,
   0xff, 0x7f, 0xff, 0x7f, 0xfe, 0x7f, 0xf8, 0x7f, 0xf8, 0x7f, 0xf0, 0x7f,
   0xc0, 0x7f, 0xc0, 0x7f, 0x80, 0x7f, 0x00, 0x7e, 0x00, 0x7c, 0x00, 0x7c,
   0x00, 0x78, 0x00, 0x60, 0x00, 0x60 };
#define heartRHalf_width 15
#define heartRHalf_height 27
static unsigned char heartRHalf_bits[] = {
   0x80, 0x07, 0xc0, 0x0f, 0xc0, 0x0f, 0xf0, 0x3f, 0xf8, 0x7f, 0xf8, 0x7f,
   0xff, 0x7f, 0xff, 0x7f, 0xff, 0x7f, 0xff, 0x7f, 0xff, 0x7f, 0xff, 0x7f,
   0xff, 0x7f, 0xff, 0x7f, 0xff, 0x3f, 0xff, 0x0f, 0xff, 0x0f, 0xff, 0x07,
   0xff, 0x01, 0xff, 0x01, 0xff, 0x00, 0x3f, 0x00, 0x1f, 0x00, 0x1f, 0x00,
   0x0f, 0x00, 0x03, 0x00, 0x03, 0x00 };
// Heart positions
const int heart_seperation = 6;
const int heart_verticalPosition = 17;
// -Water-
// Water Moisture Sensor
const int waterSensor_pin = A0;
// Water detection Limits
// const int waterSensor_AirValue = 720;   //replace the value with value when placed in air using calibration code
// const int waterSensor_WaterValue = 430; // minimum level for water
const int waterSensor_MaxDryValue = 700;   //replace the value with value when placed in air using calibration code
const int waterSensor_WaterValue = 400; // minimum level for water
// Level meanings, above that value is what is meant
const int waterSensor_SuperMoist = 530;
const int waterSensor_Moist = 550;
const int waterSensor_KindaDry = 650;
const int waterSensor_Dry = 670;
const int waterSensor_SuperDry = 690;
const int waterSensor_dead = 720;
int waterSensor_value = 0;
int waterSensor_history[] = {1,2,3,4}; // begin fully moist
int waterSensor_historyPos = 0;
unsigned int waterSensor_historyLastTime = 1000;
int is_waterSensor_waterIncrease = false;
DateTime waterSensor_waterIncreaseTime;
// -Touch-
const int touchSensor_pin = D5;
// -EEPROM-
// Day to store when it was last watered
const int address_dayWatered = 0;
// -Inner Logic-
// current event
enum EVENTMODE {
  WAIT_CYCLE,
  CAN_BE_WATERED_CYCLCE,
  NEEDS_WATER_CYCLE,
  NEAR_DEATH_CYCLE,
  WATERED
};
EVENTMODE currentEvent = WAIT_CYCLE;
// Display modes
enum DISPLAYMODE {
  VISIBLE,
  HAPPYNESS,
  DANGER,
  CLEAR
};
DISPLAYMODE currentDisplay = CLEAR;

// Setup
// -----
void setup() {
  // initialize serial communications at 9600 bps:
  Serial.begin(9600);
  // initialize EEPROM
  EEPROM.begin(512);

  #ifndef ESP8266
    while (!Serial); // wait for serial port to connect. Needed for native USB
  #endif
  
  if (! rtc.begin()) {
    Serial.println("Couldn't find RTC");
    Serial.flush();
    while (1) delay(10);
  }

  // initialize oled display
  u8g2.begin();

  // Touch Sensor
  pinMode(touchSensor_pin, INPUT);
}


// Input/Read Functions
// ---------------
void readCurrentTime() {
  currentTime = rtc.now();
  lastTime = readLastTime();
}

void readMoistureSensor() {
  waterSensor_value = analogRead(waterSensor_pin);
}

// Reads the EEPROM for the lastTime
DateTime readLastTime() {
    //uint32_t unix;
    //EEPROM.get(1, unix); // store value into unix variable
    //DateTime lastTime(unix); // convert to DateTime
    DateTime lastTime; 
    rtc.readnvram((uint8_t *) &lastTime, sizeof(lastTime), 0);
    return lastTime;
}

// Reads the EEPROM for the lastTime as UNIX
//DateTime readLastTimeUnix() {
//    uint32_t unix;
//    EEPROM.get(1, unix); // store value into unix variable
//    return unix;
//}

// Functions
// ---------

// Either map or if statements
int getHeartMappedStatus() {
  return map(waterSensor_value, waterSensor_MaxDryValue, waterSensor_WaterValue, 0, 6);
}

int getHeartStatus() {
  if (waterSensor_value > waterSensor_dead) {
    return 0;
  } else if (waterSensor_value > waterSensor_SuperDry) {
    return 1;
  } else if (waterSensor_value > waterSensor_Dry) {
    return 2;
  } else if (waterSensor_value > waterSensor_KindaDry) {
    return 3;
  } else if (waterSensor_value > waterSensor_Moist) {
    return 4;
  } else if (waterSensor_value > waterSensor_SuperMoist) {
    return 5;
  } else {
    return 6;
  }
}

// Displays a Full, Half or Empty heart at x,y
void displayHeart(int x, int y, int heartMode) {
  u8g2.setBitmapMode(true);
  u8g2.drawXBMP(x, y, heartShell_bits_width, heartShell_bits_height, heartShell_bits);
  // Serial.print("Display: heart mode = ");
  // Serial.println(heartMode);
  switch (heartMode) {
    case FULL: {
      u8g2.drawXBMP(x+16, y+3, heartRHalf_width, heartRHalf_height, heartRHalf_bits);
    }
    case HALF: {
      u8g2.drawXBMP(x+3, y+3, heartLHalf_width, heartLHalf_height, heartLHalf_bits);
      break;
    }
    case EMPTY: {
      break;
    }
  }
}


void displayHearts() {
  int hearts = getHeartStatus();
  u8g2.firstPage();    
  do {
  displayHeart(1+ heart_seperation,                             17, min(hearts,2));
  displayHeart(1+ heart_seperation*2 + heartShell_bits_width,   17, min(max(hearts-2,0),2));
  displayHeart(1+ heart_seperation*3 + heartShell_bits_width*2, 17, min(max(hearts-4,0),2));
  u8g2.drawLine(0, 63, 127, 63);
  u8g2.drawLine(0, 0, 128, 0);
  u8g2.drawLine(0, 0, 0, 63);
  u8g2.drawLine(127, 0, 127, 63);
  } while( u8g2.nextPage() );
}

void displayInfo() {
  int hearts = getHeartStatus();
  u8g2.firstPage();    
  do {
  u8g2.setFont(u8g2_font_ncenB08_tr);
  u8g2.setCursor(2, 10);
  u8g2.printf("water: %i", waterSensor_value);
  u8g2.setCursor(2, 60);
  //u8g2.printf("%i.%i.%i     %i.%i.%i", currentTime.day(), currentTime.month(), currentTime.year(), lastTime.day(), lastTime.month(), lastTime.year());
  u8g2.printf("Days Remaining: %i", ((lastTime+TimeSpan(6,0,0,0)) - currentTime).days() );
  
  displayHeart(1+ heart_seperation,                             17, min(hearts,2));
  displayHeart(1+ heart_seperation*2 + heartShell_bits_width,   17, min(max(hearts-2,0),2));
  displayHeart(1+ heart_seperation*3 + heartShell_bits_width*2, 17, min(max(hearts-4,0),2));
  u8g2.drawLine(0, 63, 127, 63);
  u8g2.drawLine(0, 0, 128, 0);
  u8g2.drawLine(0, 0, 0, 63);
  u8g2.drawLine(127, 0, 127, 63);
  } while( u8g2.nextPage() );
}

void displayNothing() {
  u8g2.firstPage();    
  do {} while( u8g2.nextPage() );
}

//void saveLastTime(uint32_t unix) {
//  Serial.println("-----SavedTime");
//  EEPROM.put(1, unix);
//}

void saveLastTime(DateTime time) {
  rtc.writenvram(0, (uint8_t *) &time, sizeof(time));
}

// Check if the point in time combined with time remaining is over.
bool isTimeOver(DateTime lastTime, TimeSpan timeRemaining) {
  // calculate the future. last time combined with time remaining
  DateTime future (lastTime + timeRemaining);
  Serial.print("-----lastTime =");
  Serial.println(lastTime.timestamp());
  Serial.print("-----currentTime =");
  Serial.println(currentTime.timestamp());
    Serial.print("-----targetTime =");
  Serial.println(future.timestamp());
  // if the future that should|| if lastTime is in the future
  // come, is now in the past || then ignore lastTime because its broken, shouldn't happen
  return future < currentTime || lastTime > currentTime;
}

// Check if the point in time combined with time remaining is not over.
bool isInTime(DateTime lastTime, TimeSpan timeRemaining) {
  // calculate the future. last time combined with time remaining
  DateTime future (lastTime + timeRemaining);
  // if the future that should && if lastTime is in the future
  // come, is not in the past  && then ignore lastTime because its broken, shouldn't happen
  return  future > currentTime && lastTime <= currentTime+TimeSpan(0,0,0,1);
}

// Logs water into an array of 4
// Automatically shifts the array and its cursor
void logWaterHistory(int waterLevel) {
  // Shift one position
  waterSensor_historyPos = (waterSensor_historyPos+1)%4;
  // Store water level into with the cursor into the array
  waterSensor_history[waterSensor_historyPos] = waterLevel;
}


// Compares if the water from 1 seconds ago is lower than currently.
// Dynamic way to detect Watering.
// Add a tolerance value to it to not miss-detect random water increases from a value of 10.
bool isCurrentWaterMoist(int tolerance) {
  // Is water suddenly more?
  // Compares current WaterLevel (first pos in history) vs (fourth point in history)
  int currentWater = waterSensor_history[waterSensor_historyPos] + tolerance;
  int oldWater = waterSensor_history[(waterSensor_historyPos+1)%4];
  // water comparison
  //Serial.print("Water Comparison = "); 
  //Serial.print(currentWater);
  //Serial.print(" < ");
  //Serial.println(oldWater);
  if (currentWater < oldWater) {
    return true;
  }
  return false;
}

// Compares if the water from 4 seconds ago is lower than currently.
// Add a tolerance value to it to not miss-detect random water increases from a value of 10.
bool isWaterOverTimeMoist(int tolerance) {
  // Is water over time more?
  // Compares current WaterLevel (first pos in history) vs (second point in history)
  int currentWater = waterSensor_history[waterSensor_historyPos] + tolerance;
  int oldWater = waterSensor_history[(waterSensor_historyPos+3)%4];
  // older water comparison
  //Serial.print("Water over time Comparison = "); 
  //Serial.print(currentWater);
  //Serial.print(" < ");
  //Serial.println(oldWater);
  if ( currentWater < oldWater ) {
    return true;
  }
  return false;
}

// Check and Store current time into specific variable
void checkWaterIncrease() {
  // Water increased or is below minimum
  if (isCurrentWaterMoist(20) || isWaterOverTimeMoist(30)) { //|| waterSensor_history[waterSensor_historyPos] < waterSensor_WaterValue) {
    //Serial.println(isCurrentWaterMoist(20));
    //Serial.println(isWaterOverTimeMoist(30)); 
    //Serial.println(waterSensor_history[waterSensor_historyPos] < waterSensor_WaterValue);
    Serial.println("Current soil is moist");
    // Store current Time 
    waterSensor_waterIncreaseTime = currentTime;
    is_waterSensor_waterIncrease = true;
  }
}

void printAllInfos() {
  // Event
  Serial.print("CurrentEvent = ");
  Serial.println(currentEvent);
  // Display status
  Serial.print("DisplayStatus = ");
  Serial.println(currentDisplay);
  // history
  Serial.print("History = ");
  for (int i = 0; i < 4; i++) {
    Serial.print(waterSensor_history[i]);
    Serial.print(", ");
  }
  Serial.println("");
  // current water
  Serial.print("Water = ");
  Serial.println(waterSensor_value);
  // Time
  Serial.print("CurrentTime = ");
  Serial.println(currentTime.timestamp());
  // LastTime
  Serial.print("LastTime = ");
  Serial.println(lastTime.timestamp());
  // next water time
  Serial.print("waterSensor_waterIncreaseTime = ");
  Serial.println(waterSensor_waterIncreaseTime.timestamp());
}

// Loop
// ----
void loop() {
  // RTC Clock can only communicate with 100khz while u8g2 sets it on 400khz everytime possible
  Wire.setClock(100000);
  // get real time into currentTime
  readCurrentTime();
  // get waterSensor value from analog input 
  readMoistureSensor();
  // Log into history every second
  if (millis() - waterSensor_historyLastTime > 998) {
    waterSensor_historyLastTime = millis();
    logWaterHistory(waterSensor_value);
  }

  // Debug
  printAllInfos();

  // check if water increased a little bit, stores time into specific variable
  checkWaterIncrease();

  // If water increased, display for 20 seconds the display
  // Checks the specific variable if its *not* over 20 seconds ago.
  // Jump to WATERED if moist enough 
  if (is_waterSensor_waterIncrease && isInTime(waterSensor_waterIncreaseTime, TimeSpan(0,0,0,20))) {

    Serial.println("20 seconds display time");
    // Display some flashing or happyness
    currentDisplay = HAPPYNESS;
    // Check if water increased enough to count as watered, jump to WATERED if yes, if suddenly it went dry then change to dry
    if (waterSensor_value < waterSensor_SuperMoist) {
      currentEvent = WATERED;
    } else if (waterSensor_value > waterSensor_SuperDry) {
      currentEvent = NEAR_DEATH_CYCLE;
    }

    // if 20 seconds are over and there is no water increase anymore
  } else if (is_waterSensor_waterIncrease) {

    // No increase anymore, set it to false
    is_waterSensor_waterIncrease = false;
    // Store to EEPROM last time it was watered
    saveLastTime(currentTime);
    // Just in case, just make it clear
    currentDisplay = CLEAR;

  }

  switch (currentEvent) {
    case WAIT_CYCLE: {

      // if its after 6 days, jump to CAN_BE_WATERED_CYCLCE. Demo 1 minute
      if (isTimeOver(readLastTime(), TimeSpan(6,0,0,0))) { 
        currentEvent = CAN_BE_WATERED_CYCLCE;
      }
      break;

    }
    case CAN_BE_WATERED_CYCLCE: {

      // if sensors detects dry, jump to NEEDS_WATER_CYCLE.
      if (waterSensor_value > waterSensor_Dry) {
        currentEvent = NEEDS_WATER_CYCLE;
      }
      break;

    }
    case NEEDS_WATER_CYCLE: {

      // Render Display to annoy person.
      currentDisplay = VISIBLE;
      // Near death means... what does it mean? Just blink, Human should prevent plant death. Annoy even more!
      if (waterSensor_value > waterSensor_SuperDry) {
        currentEvent = NEAR_DEATH_CYCLE;
      }
      break;

    }
    case NEAR_DEATH_CYCLE: {

      // TODO: implement blink
      currentDisplay = DANGER;
      break;

    }
    case WATERED: {
      // you only come here if plant is watered and lastTime is saved      
      if (is_waterSensor_waterIncrease == false) {
        // Display normally
        currentDisplay = VISIBLE;

        // if 1 minute is over
        if (isTimeOver(readLastTime(), TimeSpan(0,0,1,0))) {
          // clear display
          currentDisplay = CLEAR;
          currentEvent = WAIT_CYCLE;
        }
      }
      break;

    }
  }

  if (digitalRead(touchSensor_pin)) {
    displayInfo();
  } else {
    switch (currentDisplay) {
      case VISIBLE: {
        displayHearts();
        break;
      }
      case HAPPYNESS: {
        static bool switchState = false;
        static unsigned long lastMillis = millis();
        if (millis() - lastMillis > 500) {
          switchState = !switchState;
          lastMillis = millis();
        }

        if (switchState) {
          displayHearts();
        } else {
          displayNothing();
        }
              
        break;
      }
      case DANGER: {
        static bool switchState = false;
        static unsigned long lastMillis = millis();
        if (millis() - lastMillis > 100) {
          switchState = !switchState;
          lastMillis = millis();
        }

        if (switchState) {
          displayHearts();
        } else {
          displayNothing();
        }

        break;
      }
      case CLEAR: {
        displayNothing();
        break;
      }
    }
  }

  // Save the electricity bill or smth, maybe it does no difference
  switch (currentEvent) {
    case WAIT_CYCLE:
    case CAN_BE_WATERED_CYCLCE: 
      delay(1000);
      break;
    case NEEDS_WATER_CYCLE:
    case NEAR_DEATH_CYCLE:
    case WATERED: 
      delay(100);
      break;
  }
  yield();
}


